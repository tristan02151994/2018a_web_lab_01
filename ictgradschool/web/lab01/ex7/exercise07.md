#Herbivorous
* Horse
* Cow
* Sheep
* Frog
#Carnivorous
* Dog
* Cat
* Polar Bear
* Hawk
* Lion
#Omnivorous
* Rat
* Fox
* Capybara
* Raccoon
* Chicken
* Fennec Fox